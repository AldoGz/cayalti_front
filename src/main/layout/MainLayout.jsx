import { Footer } from "../components/Footer/Footer";
import { Header } from "../components/Header/Header";

export const MainLayout = ({ children }) => {
  return (
    <>
      <Header></Header>
      <div style={{ minHeight: "500px" }}>{children}</div>
      <Footer></Footer>
    </>
  );
};
