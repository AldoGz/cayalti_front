import React from "react";
import { MDBContainer, MDBRow, MDBCol } from "mdb-react-ui-kit";
import { GridTitle } from "./";

/* ACA SE VA ALIMENTAR DE FOTOS ORIGINALES */
const images = [
  {
    id: 1,
    title: "Enlace a SMV",
  },
  {
    id: 2,
    title: "Inf. Financiera",
  },
  {
    id: 3,
    title: "Memoria Anual",
  },
  {
    id:4,
    title: "Trabaja con Nosotros",
  },
];

/* ACA SE VA ALIMENTAR DE FOTOS ORIGINALES */

export const LinkMenu = () => {
  return (
    <MDBContainer>
      <MDBRow className="row-cols-1 row-cols-md-4 g-4">
        {images.map(({ id, title }) => (
          <MDBCol key={id}>
            <GridTitle title={title}></GridTitle>
            
          </MDBCol>
        ))}
      </MDBRow>
    </MDBContainer>
  );
};
