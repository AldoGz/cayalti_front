import React from 'react'

export const GridTitle = ({title="title"}) => {
  return (
    <div className="text-center">{title}</div>
  )
}
