import React from "react";
import { ButtonTranslations } from "./ButtonTranslations";

export const GridButtonTranslations = () => {
  return (
    <div className="position-absolute header-button-position">
      <ButtonTranslations></ButtonTranslations>
    </div>
  );
};
