import { MDBBtn } from "mdb-react-ui-kit";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";

export const BackButton = ({ imagen = "" }) => {
  const [mouseOver, setMouseOver] = useState(false);
  const [t] = useTranslation("global");
  const navigate = useNavigate();
  

  const MouseOver = () => {
    setMouseOver(true);
  };
  const MouseOut = () => {
    setMouseOver(false);
  };

  const hanldBackButton = () => {
    navigate("/", { replace: true });
  }
  return (
    <div>
      <MDBBtn
        className="text-center back-button"
        floating
        color="none"
        tag="a"
        onMouseOver={MouseOver}
        onMouseOut={MouseOut}
        onClick={hanldBackButton}
      >
        <img
          src={
            "https://i.pinimg.com/736x/3a/33/c1/3a33c11780a0cc4d69a8ff1f4d1095c4.jpg"
          }
          className="img-fluid"
          style={{
            opacity: mouseOver && "0.25"
          }}
          alt={"TEST"}
        />
        {mouseOver && (
          <div className="mask">
            <div className="d-flex justify-content-center align-items-center h-100">
              <p className="mb-0 mx-3 back-button-format">{t("header.history")}</p>
            </div>
          </div>
        )}
      </MDBBtn>
    </div>
  );
};
