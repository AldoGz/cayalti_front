import React from "react";
import { MDBContainer, MDBIcon, MDBBtn } from "mdb-react-ui-kit";
import { GridButtonTranslations } from "./GridButtonTranslations";

export const Header = () => {
  return (
    <header>
      <MDBContainer fluid></MDBContainer>
      <div
        id="intro-example"
        className="p-5 text-center bg-image"
        style={{
          backgroundImage:
            "url('https://media.istockphoto.com/videos/abstract-blocks-background-video-id1191991111?b=1&k=20&m=1191991111&s=640x640&h=hj4bkJc7S1wwnjSnZ6_KZ2P2z1wjTXhkECA3msDfGv4=')",
        }}
      >
        <div className="mask">
            <img src="https://upload.wikimedia.org/wikipedia/commons/a/a6/PlayStation_black_logo.png" alt="Wild Landscape" className="logo img-fluid"/>
            </div>
            <GridButtonTranslations>
            </GridButtonTranslations>
      </div>
    </header>
  );
};
