import { useTranslation } from "react-i18next";

export const ButtonTranslations = () => {
  const [ , i18n] = useTranslation("global");
  return (
    <small>
      <div
        className="d-inline py-1 cursor-pointer left-language"
        onClick={() => i18n.changeLanguage("es")}
      >
        ES
      </div>
      <div
        className="d-inline py-1 cursor-pointer right-language"
        onClick={() => i18n.changeLanguage("en")}
      >
        EN
      </div>
    </small>
  );
};
