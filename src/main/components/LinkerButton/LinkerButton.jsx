import React from "react";
import { MDBContainer, MDBRow, MDBCol } from "mdb-react-ui-kit";
import { GridLinkerButton } from "./";

/* ACA SE VA ALIMENTAR DE FOTOS ORIGINALES */
const images = [
  {
    id: 1,
    imagen: "https://mdbootstrap.com/img/new/standard/city/001.webp",
  },
  {
    id: 2,
    imagen: "https://mdbootstrap.com/img/new/standard/city/002.webp",
  },
  {
    id: 3,
    imagen: "https://mdbootstrap.com/img/new/standard/city/003.webp"
  },
];

/* ACA SE VA ALIMENTAR DE FOTOS ORIGINALES */

export const LinkerButton = () => {
  return (
    <MDBContainer>
      <MDBRow className="row-cols-1 row-cols-md-3 g-4">
        {images.map(({ id, imagen }) => (
          <MDBCol key={id}>
            <GridLinkerButton imagen={imagen}></GridLinkerButton>
          </MDBCol>
        ))}
      </MDBRow>
    </MDBContainer>
  );
};
