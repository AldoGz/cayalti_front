import { MDBCard, MDBCardImage } from "mdb-react-ui-kit";

export const GridLinkerButton = ({imagen}) => {
  return (
    <MDBCard className=" mx-auto my-4" style={{ maxWidth:100}}>
      <MDBCardImage style={{ height: 100 }} src={imagen} alt="imagen-product" />
    </MDBCard>
  );
};
