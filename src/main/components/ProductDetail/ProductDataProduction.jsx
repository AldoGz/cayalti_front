import React from "react";
import { MDBContainer, MDBRow, MDBCol } from "mdb-react-ui-kit";
import { useTranslation } from "react-i18next";

export const ProductDataProduction = ({ actives }) => {
  const [t, i18n] = useTranslation("global");

  return (
    <MDBRow className="text-center text-white pt-3 mx-auto">
      <MDBCol size={12} className="text-center mb-3">Producción</MDBCol>
      {[
        "enero",
        "febrero",
        "marzo",
        "abril",
        "mayo",
        "junio",
        "julio",
        "agosto",
        "septiembre",
        "octubre",
        "noviembre",
        "diciembre",
      ].map((text, index) => (
        <MDBCol size="md" key={index} className="d-flex flex-column justify-content-center align-items-center mb-3 mx-3">
          {actives[index] ? (
            <div className="production-active">
              <div className="production-active-inline"></div>
            </div>
          ) : (
            <div className="production-inactive"></div>
          )}
          <div>{t(`calendario.${text}`)}</div>
        </MDBCol>
      ))}
    </MDBRow>
  );
};
