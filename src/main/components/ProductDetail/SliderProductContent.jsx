import React from "react";

export const SliderProductContent = ({text}) => {
  return (
    <div className="mask">
      <div className="d-flex justify-content-center align-items-center h-100">
        <p className="text-white mb-0">{text}</p>
      </div>
    </div>
  );
};
