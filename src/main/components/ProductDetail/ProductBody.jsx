import { MDBRow, MDBCol } from "mdb-react-ui-kit";

export const ProductBody = ({ imagenA = "", text, imagenB }) => {
  return (
    <MDBRow>
      <MDBCol size="md">
        <figure className="figure">
          <img
            src={imagenA}
            className="figure-img img-fluid rounded shadow-3 mb-3"
            alt="imagen"
          />
        </figure>
      </MDBCol>
      <MDBCol size="md">
        <p style={{textAlign:"justify"}}>
          { text }
        </p>
      </MDBCol>
      <MDBCol size="md">
        <figure className="figure">
          <img
            src={imagenB}
            className="figure-img img-fluid rounded shadow-3 mb-3"
            alt="imagen"
          />
        </figure>
      </MDBCol>
    </MDBRow>
  );
};
