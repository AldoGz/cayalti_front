import {
  MDBCarousel,
  MDBCarouselInner,
  MDBCarouselItem,
  MDBCarouselElement,
  MDBCarouselCaption,
  MDBBtn,
  MDBIcon,
} from "mdb-react-ui-kit";

import { useEffect, useState } from "react";
import { SliderProductContent } from "./SliderProductContent";

const imagenes = [
  {
    id: 1,
    imagen: "https://mdbootstrap.com/img/Photos/Slides/img%20(15).webp",
    text: "Mandarina",
  },
  {
    id: 2,
    imagen: "https://mdbootstrap.com/img/Photos/Slides/img%20(22).webp",
    text: "Pera",
  },
  {
    id: 3,
    imagen: "https://mdbootstrap.com/img/Photos/Slides/img%20(23).webp",
    text: "Arandano",
  },
];

export const SliderProduct = () => {
  return (
    <MDBCarousel showIndicators className="style-indicators">
      <MDBCarouselInner>
        {imagenes.map(({ id, imagen, text }, index) => (
          <MDBCarouselItem className={index == 0 && "active"} key={id}>
            <MDBCarouselElement src={imagen} alt="imagen" />
            <SliderProductContent text={text}/>            
          </MDBCarouselItem>
        ))}
      </MDBCarouselInner>
    </MDBCarousel>
  );
};
