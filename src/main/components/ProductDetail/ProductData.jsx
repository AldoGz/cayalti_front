import React from "react";
import { ProductDataProduction } from "./ProductDataProduction";
import { MDBCard, MDBRow, MDBCol } from "mdb-react-ui-kit";

export const ProductData = ({ imagenA = "", imagenB = "" }) => {
  return (
    <MDBCard className="my-4">
      <MDBRow className="g-0">
        <MDBCol size="md">
          <figure className="figure">
            <img
              src={imagenA}
              className="figure-img img-fluid rounded shadow-3"
              alt="imagen"
            />
          </figure>
        </MDBCol>
        <MDBCol size="md" style={{backgroundColor:'green'}}>
          <ProductDataProduction
            actives = {
              [false, false,false,false,false, false,false,false,false, true,true,true]
            }
          ></ProductDataProduction>
        </MDBCol>
        <MDBCol size="md">
          <figure className="figure">
            <img
              src={imagenB}
              className="figure-img img-fluid rounded shadow-3"
              alt="imagen"
            />
          </figure>
        </MDBCol>
      </MDBRow>
    </MDBCard>
  );
};
