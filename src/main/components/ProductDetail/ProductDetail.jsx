import { ProductBody } from "./ProductBody";
import { ProductData } from "./ProductData";
import { SliderProduct } from "./SliderProduct";

export const ProductDetail = () => {
  return (
    <>
      <SliderProduct></SliderProduct>
      <br />
      <ProductBody
        imagenA={"https://mdbootstrap.com/img/new/standard/city/041.webp"}
        imagenB={"https://mdbootstrap.com/img/new/standard/city/041.webp"}
        text={
          "Non ad irure adipisicing excepteur elit ad. Lorem consectetur tempor ut occaecat do commodo pariatur mollit."
        }
      ></ProductBody>
      <br />
      <ProductData
        imagenA={"https://mdbootstrap.com/img/new/standard/city/041.webp"}
        imagenB={"https://mdbootstrap.com/img/new/standard/city/041.webp"}
      ></ProductData>
    </>
  );
};
