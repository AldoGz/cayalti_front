import {
  MDBCard,
  MDBCardTitle,
  MDBRow,
  MDBCol
} from "mdb-react-ui-kit";

export const GridTitle = ({ title = "Title", icon = "icon.png"}) => {
  return (
    <MDBCard className="my-4">
      <MDBRow className="g-0">
        <MDBCol md="4">
          <img
            src={icon}
            className="img-fluid"
            alt="Icon"
          />
        </MDBCol>
        <MDBCol md="8">
          <MDBCardTitle className="d-flex align-items-center justify-content-center" style={{ height:'100%' }}>{title}</MDBCardTitle>
        </MDBCol>
      </MDBRow>
    </MDBCard>
  );
};
