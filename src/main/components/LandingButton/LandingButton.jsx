import React from "react";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
} from "mdb-react-ui-kit";
import { GridIcon } from "./GridIcon";
import { GridTitle } from "./GridTitle";



/* ACA SE VA ALIMENTAR DE FOTOS ORIGINALES */
const images = [
  {
    id: 1,
    imagen: "https://mdbootstrap.com/img/new/standard/city/001.webp",
    title : "Nosotros",
    icon : "https://mdbootstrap.com/img/new/standard/city/001.webp"
  },
  {
    id: 2,
    imagen: "https://mdbootstrap.com/img/new/standard/city/002.webp",
    title : "Productos",
    icon: "https://mdbootstrap.com/img/new/standard/city/002.webp",
  },
  {
    id: 3,
    imagen: "https://mdbootstrap.com/img/new/standard/city/003.webp",
    title : "Comunidad",
    icon: "https://mdbootstrap.com/img/new/standard/city/003.webp",
  },
];

/* ACA SE VA ALIMENTAR DE FOTOS ORIGINALES */

export const LandingButton = () => {

  
  return (
    <MDBContainer>
      <MDBRow className="row-cols-1 row-cols-md-3 g-4">
        {images.map(({ id, imagen, title, icon }) => (
          <MDBCol key={id}>
            <GridTitle title={title} icon={icon}></GridTitle>
            <GridIcon imagen={imagen} ></GridIcon>
          </MDBCol>
        ))}
      </MDBRow>
    </MDBContainer>
  );
};
