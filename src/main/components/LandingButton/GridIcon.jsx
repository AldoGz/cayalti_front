import { MDBCard, MDBCardImage } from "mdb-react-ui-kit";

import { useNavigate } from "react-router-dom";

export const GridIcon = ({imagen}) => {
  const navigate = useNavigate();
  const hanldAbout = () => {
    navigate("/about", { replace: true });
  }

  return (
    <MDBCard className="my-4" >
      <MDBCardImage className="cursor-pointer" style={{ height: 350 }} src={imagen} alt="imagen-product" onClick={hanldAbout}/>
    </MDBCard>
  );
};
