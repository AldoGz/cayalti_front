import { ButtonRedes } from "./";
import { useState } from "react";


export const GridSocialNetworks = () => {
  const [mouseOver, setMouseOver] = useState(false);
  
  const MouseOver = () => {
    setMouseOver(true);
  }
  const MouseOut = () =>{
    setMouseOver(false);
  }
  /* REVISARLO */
  const hanldToogle = () => {
    setMouseOver(true);
  }

  return (
    <div className="d-flex px-3">
      <div className="w-100 pe-2">
        <div className="footer-dividir"></div>
      </div>
      <div className="flex-shrink-1" onMouseOver={MouseOver} onMouseOut={MouseOut} >
        <ButtonRedes open={mouseOver}></ButtonRedes>
      </div>
    </div>
  );
};
