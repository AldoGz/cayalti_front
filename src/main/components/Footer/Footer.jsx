import { MDBFooter } from "mdb-react-ui-kit";

import { GridSocialNetworks , GridContacts } from "./";

export const Footer = () => {
  
  return (
    <MDBFooter bgColor="light">
      <div className="position-relative"  >
        <div className="position-absolute footer-position">
          <GridSocialNetworks/>
        </div>
      </div>
      <GridContacts></GridContacts>
    </MDBFooter>
  );
};
