import { useTranslation } from "react-i18next";
import { MDBCol, MDBContainer, MDBRow } from "mdb-react-ui-kit";


export const GridContacts = () => {
  const [t] = useTranslation("global");
  return (
    <MDBContainer className="text-center text-lg-start pt-2">
      <MDBRow className="mt-3">
        <MDBCol md="3" lg="5" xl="5" xxl="5" className="mx-auto mb-3">
          <h6 className="fw-bold">{t("footer.oficina-cayalti")}</h6>
          <div className="small">Av. Elbilverto Rivas</div>
          <div className="small">Vásquez S/N</div>
        </MDBCol>

        <MDBCol md="3" lg="5" xl="5" xxl="5" className="mx-auto">
          <h6 className="fw-bold">{t("footer.oficina-chiclayo")}</h6>
          <p className="small">Av. Loreto #165</p>
        </MDBCol>

        <MDBCol md="3" lg="2" xl="2" xxl="2" className="mx-auto mb-3">
          <h6 className="fw-bold">{t("footer.contacto")}</h6>
          <div className="small">074 42 13 64</div>
          <div className="small">074 20 72 44</div>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
};
