import { MDBCollapse, MDBBtn, MDBBadge } from "mdb-react-ui-kit";
import { useTranslation } from "react-i18next";

const redes = [
  {
    name: "youtube",
    ruta: "",
    icon: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/YouTube_social_red_circle_%282017%29.svg/1200px-YouTube_social_red_circle_%282017%29.svg.png",
    type: "fab",
  },

  {
    name: "linkedin",
    ruta: "",
    icon: "https://upload.wikimedia.org/wikipedia/commons/f/f8/LinkedIn_icon_circle.svg",
    type: "fab",
  },
  {
    name: "tiktok",
    ruta: "",
    icon: "https://i.pinimg.com/originals/e1/0e/3f/e10e3f21d3b4e0f40b04b8fee7f40da4.png",
    type: "fab",
  },

  {
    name: "twitter-square",
    ruta: "",
    icon: "https://e7.pngegg.com/pngimages/892/42/png-clipart-computer-icons-desktop-social-media-logo-twitter-internet-logos-thumbnail.png",
    type: "fab",
  },
  {
    name: "instagram",
    ruta: "",
    //icon: "https://www.pnglib.com/wp-content/uploads/2021/02/instagram-logo-png_6023f9ae0feb9.png",
    icon: "https://nickmarketing.co/wp-content/uploads/2021/02/icono-instagram.png",
    type: "fab",
  },
  {
    name: "whatsapp",
    ruta: "",
    icon: "https://e7.pngegg.com/pngimages/347/373/png-clipart-computer-icons-whatsapp-whatsapp-text-trademark.png",
    type: "fab",
  },
  {
    name: "facebook-f",
    ruta: "",
    icon: "https://pngset.com/images/social-network-media-new-2019-logo-icon-circle-facebook-icon-first-aid-symbol-trademark-word-transparent-png-2530027.png",
    type: "fab",
  },
];

export const ButtonRedes = ({open }) => {
  const [t, i18n] = useTranslation("global");
  const [ type ] = i18n.languages;

  const styles = {
    position: "absolute",
    transform: `translate(${type == "es" ? "8px" : "21px"}, -${redes.length * 45}px)`,
  };
  return (
    <>
      <MDBCollapse show={open} style={styles}>
        {redes.map((red) => (
          <div className="d-block pb-2" key={red.name}>
            <MDBBtn floating color="none" tag="a">
              <img src={red.icon} className="img-fluid" alt={red.name} />
            </MDBBtn>
          </div>
        ))}
      </MDBCollapse>
      <MDBBadge className="cursor-pointer" color="success">
        {t("footer.redes")}
      </MDBBadge>
    </>
  );
};
