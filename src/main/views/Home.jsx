import React from "react";
import { MainLayout } from "../layout/MainLayout";
import { LandingButton } from "../components/LandingButton/LandingButton";
import { LinkerButton } from "../components/LinkerButton";
import { LinkMenu } from "../components/LinkMenu";
import { BackButton } from "../components/Header/BackButton";


export const Home = () => {
  return (
    <MainLayout>
      <BackButton></BackButton>
      <LandingButton></LandingButton>
      <LinkerButton></LinkerButton>
      <LinkMenu></LinkMenu>
    </MainLayout>
  );
};
