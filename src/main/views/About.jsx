import React from "react";
import { MainLayout } from "../layout/MainLayout";
import { LandingButton } from "../components/LandingButton/LandingButton";
import { LinkerButton } from "../components/LinkerButton";
import { LinkMenu } from "../components/LinkMenu";
import { BackButton } from "../components/Header/BackButton";
import { ProductDetail } from "../components/ProductDetail";


export const About = () => {
  return (
    <MainLayout>
      <BackButton></BackButton>
      <ProductDetail></ProductDetail> 
      
    </MainLayout>
  );
};
