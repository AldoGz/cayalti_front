import { AppRouter } from "./main/routes/AppRouter";

export default function App() {
  return <AppRouter></AppRouter>;
}